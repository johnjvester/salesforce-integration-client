# Salesforce Integration Application (`salesforce-integration-client`)

> The Salesforce Integration Spring Boot Example Application (`salesforce-integration-client`) is a [Angular](<https://angular.io/>) based 
> application, utilizing the [High Charts Angular](<https://github.com/highcharts/highcharts-angular>) wrapper for [High Charts](https://www.highcharts.com/) - 
> which utilizes the [`salesforce-integration-api`](<https://gitlab.com/johnjvester/salesforce-integration-api>) repository, to provide 
> a full-stack solution for presenting time data located in [Salesforce](https://www.salesforce.com/) within a burndown chart.

![Angular Client Example](./AngularClientExample.png)

This replaces use of a manually-updated Microsoft Excel file:

![Excel Example](./ExcelBurndownChart.png)

## Publications

This repository is related to an article published on DZone.com:

* [Integrating Traditional Cloud Development with Salesforce](https://dzone.com/articles/integrating-traditional-cloud-development-with-sal)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

# Angular Client Setup

After following the instructions listed in the [`salesforce-integration-api`](<https://gitlab.com/johnjvester/salesforce-integration-api>) repository, the 
only update here is to make the necessary changes to the `api` attribute in the `environments` package.  Below, is an example of the `environment.ts` file 
using a local instance of `salesforce-integration-api` running on Spring Boot (default) port `8080`.

```
export const environment = {
  production: false,
  api: 'http://localhost:8080'
};
```

Made with ♥ by johnjvester@gmail.com, because I enjoy writing code.


