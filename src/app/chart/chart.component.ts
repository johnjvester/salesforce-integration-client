import { Component, OnInit } from '@angular/core';
import {IntegrationService} from '../services/integration.service';
import {Time} from '../models/time';
import {Goal} from '../models/goal';
import {Month} from '../models/month';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  constructor(private integrationService:IntegrationService, ) { }

  goal: Goal;
  time: Time[];

  months: Month[];
  month: Month;
  actualHours: number[] = [];
  expectedBurn: number[] = [];
  days: string[] = [];

  ngOnInit() {
    this.getPickListData();
  }

  processPickListChange() {
    this.getData(this.month);
  }

  private getPickListData() {
    this.integrationService.getMonths()
      .subscribe(data => {
        this.months = data;
      }, (error) => {
        console.error('error', error);
      });
  }

  private getData(month: Month) {
    this.integrationService.getGoal(this.month.month, this.month.year)
      .subscribe(data => {
        this.goal = data;

        this.integrationService.getTime(this.month.month, this.month.year)
          .subscribe(data => {
            this.time = data;
            this.computeChartData();
            this.setChartOptions();

          }, (error) => {
            console.error('error', error);
          });
      }, (error) => {
        console.error('error', error);
      });
  }

  private computeChartData() {
    this.days = [];
    this.expectedBurn = [];
    this.actualHours = [];

    if (this.goal && this.goal.days) {
      let totalHours = this.goal.goal;
      let usedHours = this.goal.goal;

      for (let i = 0; i <= this.goal.days; i++) {
        this.days.push("" + i);
        this.expectedBurn.push(totalHours);

        totalHours = totalHours - this.goal.averagePerDay;
        totalHours = Math.round((totalHours + Number.EPSILON) * 100) / 100;

        if (i > 0) {
          let skip:boolean = false;
          if (this.time && this.time.length >= (i)) {
            usedHours = usedHours - this.time[i-1].hours;
            usedHours = Math.round((usedHours + Number.EPSILON) * 100) / 100;
          } else {
            usedHours = 0;
            skip = true;
          }

          if (!skip) {
            this.actualHours.push(usedHours);
          }
        } else {
          this.actualHours.push(null);
        }
      }

      this.expectedBurn[this.expectedBurn.length-1] = 0.00;
    }
  }

  private setChartOptions() {
    this.chartOptions = {
      title: {
        text: 'Burndown Chart',
        x: -20 //center
      },
      subtitle: {
        text: this.month.displayName,
        x: -20
      },
      xAxis: {
        title: {
          text: 'Days'
        },
        categories: this.days
      },
      yAxis: {
        title: {
          text: 'Hours'
        },
        plotLines: [{
          value: 0,
          width: 1
        }]
      },
      tooltip: {
        valueSuffix: ' hours',
        shared: true
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      series: [{
        type: 'line',
        name: 'Ideal Burn',
        color: 'rgba(255,0,0,0.25)',
        lineWidth: 2,
        data: this.expectedBurn
      }, {
        type: 'line',
        name: 'Actual Burn',
        color: 'rgba(0,120,200,0.75)',
        marker: {
          radius: 6
        },
        data: this.actualHours
      }]
    };
  }
}
