import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Goal} from '../models/goal';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';
import {Time} from '../models/time';
import {Month} from '../models/month';

@Injectable({
  providedIn: 'root'
})
export class IntegrationService {

  constructor(private http: HttpClient) { }
  baseUrl: string = environment.api;

  getGoal(month: number, year: number) {
    return this.http.get<Goal>(this.baseUrl + '/goal/' + year + '/' + month).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }

  getTime(month: number, year: number) {
    return this.http.get<Time[]>(this.baseUrl + '/time/' + year + '/' + month).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }

  getMonths() {
    return this.http.get<Month[]>(this.baseUrl + '/months').pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }
}
