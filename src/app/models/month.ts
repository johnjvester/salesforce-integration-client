export class Month {
  month: number;
  year: number;
  displayName: string;
}
