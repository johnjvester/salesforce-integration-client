export class Goal {
  id: number;
  month: number;
  year: number;
  goal: number;
  days: number;
  averagePerDay: number;
}
