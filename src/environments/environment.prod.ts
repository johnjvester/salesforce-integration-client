export const environment = {
  production: true,
  api: 'https://salesforce-integration-api.herokuapp.com/'
};
